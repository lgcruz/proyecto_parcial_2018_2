#Escriba su makefiile en este archivo
#
# Luis Gino Cruz Intriago
# Luigi Salvatore Basantes Zambrano
CC=gcc

OBJ1 = guarda.o llenarTabla.o claves.o valores.o contieneClave.o borrar.o remover.o get.o put.o numeroElementos.o crearHashTable.o hash.o 

OBJ2 = conexion.o conexionr.o crear.o abrir.o put_val2.o get_val2.o eliminar2.o cerrar_db2.o

OBJ3 = evaluar_Mensaje.o set_cloexec.o initserver.o abrir_db.o crear_db.o put_val.o get_val.o eliminar.o cerrar_db.o

all: dinamico logdb cliente logdb

logdb: logdb.o liblog.so libhashtab.so
	gcc -Wall -pthread obj/logdb.o lib/liblog.so lib/libhashtab.so -o bin/logdb

logdb.o: src/logdb.c 
	$(CC) -g -Wall -pthread -c src/logdb.c -I include/ -o obj/logdb.o

cliente: cliente.o liblogdb.so
	gcc -Wall obj/cliente.o lib/liblogdb.so -o bin/cliente

cliente.o: src/cliente.c 
	$(CC) -g -Wall -c src/cliente.c -I include/ -o obj/cliente.o

dinamico: prueba.o libhashtab.so
	gcc -L /lib obj/prueba.o lib/libhashtab.so -o bin/dinamico

prueba.o: src/hashtable/pruebas.c 
	$(CC) -g -Wall -c src/hashtable/pruebas.c -I include/ -o obj/prueba.o

hash.o: src/hashtable/hash.c 
	$(CC) -g -Wall -c -fPIC src/hashtable/hash.c -I include/ -o obj/hash.o

crearHashTable.o: src/hashtable/crearHashTable.c
	$(CC) -g -Wall -c -fPIC src/hashtable/crearHashTable.c -I include/ -o obj/crearHashTable.o

numeroElementos.o: src/hashtable/numeroElementos.c
	$(CC) -g -Wall -c -fPIC src/hashtable/numeroElementos.c -I include/ -o obj/numeroElementos.o

put.o: src/hashtable/put.c
	$(CC) -g -Wall -c -fPIC src/hashtable/put.c -I include/ -o obj/put.o

get.o: src/hashtable/get.c
	$(CC) -g -Wall -c -fPIC src/hashtable/get.c -I include/ -o obj/get.o

remover.o: src/hashtable/remover.c
	$(CC) -g -Wall -c -fPIC src/hashtable/remover.c -I include/ -o obj/remover.o

borrar.o: src/hashtable/borrar.c
	$(CC) -g -Wall -c -fPIC src/hashtable/borrar.c -I include/ -o obj/borrar.o

contieneClave.o: src/hashtable/contieneClave.c
	$(CC) -g -Wall -c -fPIC src/hashtable/contieneClave.c -I include/ -o obj/contieneClave.o

claves.o: src/hashtable/claves.c
	$(CC) -g -Wall -c -fPIC src/hashtable/claves.c -I include/ -o obj/claves.o

valores.o: src/hashtable/valores.c
	$(CC) -g -Wall -c -fPIC src/hashtable/valores.c -I include/ -o obj/valores.o

llenarTabla.o: src/hashtable/llenarTabla.c
	$(CC) -g -Wall -c -fPIC src/hashtable/llenarTabla.c -I include/ -o obj/llenarTabla.o

guarda.o: src/hashtable/guarda.c
	$(CC) -g -Wall -c -fPIC src/hashtable/guarda.c -I include/ -o obj/guarda.o

conexion.o: src/client/conectar_db.c
	$(CC) -g -Wall -c -fPIC src/client/conectar_db.c -I include/ -o obj/conexion.o

conexionr.o: src/client/conexion_retry.c
	$(CC) -g -Wall -c -fPIC src/client/conexion_retry.c -I include/ -o obj/conexionr.o

crear.o: src/client/crear_db.c
	$(CC) -g -Wall -c -fPIC src/client/crear_db.c -I include/ -o obj/crear.o

abrir.o: src/client/abrir_db.c
	$(CC) -g -Wall -c -fPIC src/client/abrir_db.c -I include/ -o obj/abrir.o

put_val2.o: src/client/put_val.c
	$(CC) -g -Wall -c -fPIC src/client/put_val.c -I include/ -o obj/put_val2.o

get_val2.o: src/client/get_val.c
	$(CC) -g -Wall -c -fPIC src/client/get_val.c -I include/ -o obj/get_val2.o

eliminar2.o: src/client/eliminar.c
	$(CC) -g -Wall -c -fPIC src/client/eliminar.c -I include/ -o obj/eliminar2.o

cerrar_db2.o: src/client/cerrar_db.c
	$(CC) -g -Wall -c -fPIC src/client/cerrar_db.c -I include/ -o obj/cerrar_db2.o

set_cloexec.o: src/liblog/set_cloexec.c
	$(CC) -g -Wall -c -fPIC src/liblog/set_cloexec.c -I include/ -o obj/set_cloexec.o

initserver.o: src/liblog/initserver.c
	$(CC) -g -Wall -c -fPIC src/liblog/initserver.c -I include/ -o obj/initserver.o

abrir_db.o: src/liblog/abrir_db.c
	$(CC) -g -Wall -c -fPIC src/liblog/abrir_db.c -I include/ -o obj/abrir_db.o

crear_db.o: src/liblog/crear_db.c
	$(CC) -g -Wall -c -fPIC src/liblog/crear_db.c -I include/ -o obj/crear_db.o

put_val.o: src/liblog/put_val.c
	$(CC) -g -Wall -c -fPIC src/liblog/put_val.c -I include/ -o obj/put_val.o

get_val.o: src/liblog/get_val.c
	$(CC) -g -Wall -c -fPIC src/liblog/get_val.c -I include/ -o obj/get_val.o

eliminar.o: src/liblog/eliminar.c
	$(CC) -g -Wall -c -fPIC src/liblog/eliminar.c -I include/ -o obj/eliminar.o

cerrar_db.o: src/liblog/cerrar_db.c
	$(CC) -g -Wall -c -fPIC src/liblog/cerrar_db.c -I include/ -o obj/cerrar_db.o

evaluar_Mensaje.o: src/liblog/evaluar_Mensaje.c
	$(CC) -g -Wall -c -fPIC src/liblog/evaluar_Mensaje.c -I include/ -o obj/evaluar_Mensaje.o

libhashtab.so: $(OBJ1)
	gcc -shared -fPIC obj/guarda.o obj/llenarTabla.o obj/claves.o obj/valores.o obj/contieneClave.o obj/borrar.o obj/remover.o obj/get.o obj/put.o obj/numeroElementos.o obj/crearHashTable.o obj/hash.o -o lib/libhashtab.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib

liblogdb.so: $(OBJ2)
	gcc -shared -fPIC obj/cerrar_db2.o obj/eliminar2.o obj/get_val2.o obj/put_val2.o obj/abrir.o obj/crear.o obj/conexionr.o obj/conexion.o -o lib/liblogdb.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib

liblog.so: $(OBJ3)
	gcc -shared -fPIC obj/evaluar_Mensaje.o obj/set_cloexec.o obj/initserver.o obj/abrir_db.o obj/crear_db.o obj/put_val.o obj/get_val.o obj/eliminar.o obj/cerrar_db.o -o lib/liblog.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib

.PHONY: clean
clean:
	rm -f obj/*.o lib/*.a bin/*

