#include "log.h"

//Main
void *evaluar_Mensaje(void* obj){
    char buffer[200];
    char *buff;
    base** lista;
    int *numBases;
    int clfd;
    char* directorio; 
    paquete* arg = (paquete*)obj;
    
    lista = arg->lista;
    numBases = arg->numBases;
    directorio = arg->directorio;
    clfd = arg->clfd;
    while(clfd>0){
	//int d = recv(clfd,buffer,200,0);
	//printf(" numero recv %d\n",d);
	if(recv(clfd,buffer,200,0)>0){
	    buff = buffer;
	    char delim[] = ",";
	    char *ptr = strtok(buff, delim);
	    char *opcion = "Servidor: Opcion no valida";
	    //3 es el maximo de valores
	    char **valores=(char **) malloc(sizeof(char *) * 4);
	    //valores = [clave,valor]
	    //valores = [clave]
	    //valores = [db]
	    
	    int numElementos =0;
	    while(ptr != NULL){
		valores[numElementos] = ptr;
		numElementos++;
		ptr = strtok(NULL, delim);
	    }
	    if(strcmp(valores[0],"abrir_DB") == 0){
		//printf("Entro a abrir_DB\n");
		char *nombreDB =valores[1];
		printf("Nombre de la DB:%s\n",nombreDB);
		opcion = abrir_db(valores[1], directorio,lista,numBases);
	     
	    }else if(strcmp(valores[0],"crear_DB") == 0){
		//printf("Entro a crear_DB\n");
		char *nombreDB =valores[1];
		//printf("Nombre de la DB:%s\n",nombreDB);
		opcion = crear_db(nombreDB,directorio);
		
	    }else if(strcmp(valores[0],"put_val") == 0){
		printf("Entro a put\n");
		char *clave= valores[1];
		char *valor = valores[2];
		int db = atoi(valores[3]);
		//printf("Con la clave: %s el valor: %s -%d-\n",clave,valor,db);
		//printf("--%d--\n",*numBases);
		if(db<(*numBases)){
		    opcion = put_val(clave,valor,lista[db]);
		}else{
		    opcion = "0";
		}
	       
	    }else if(strcmp(valores[0],"get_val") == 0){
		printf("Entro a get\n");
		char *clave= valores[1];
		int db = atoi(valores[2]);
		//printf("Con la clave:%s\n",clave);
		opcion = get_val(lista[db],clave);
		
	    }else if(strcmp(valores[0],"eliminar") == 0){
		printf("Entro a eliminar\n");
		char *clave= valores[1];
		int db = atoi(valores[2]);
		//printf("Con la clave:%s\n",clave);
		opcion = eliminar(lista[db],clave);
		
	    }else if(strcmp(valores[0],"cerrar_DB") == 0){
		printf("Entro a cerrar_DB\n");
		int db = atoi(valores[1]);
		opcion = cerrar_db(lista[db]);
		//printf("que paso karnal\n");
		
		send(clfd, opcion, strlen(opcion), 0);
		close(clfd);
		return NULL;
	    }
	    send(clfd, opcion, strlen(opcion), 0);
	    //printf("sera %p\n",p);
	    memset(buffer,0,200);
	}
    }
    close(clfd);
    return NULL;
    
}
