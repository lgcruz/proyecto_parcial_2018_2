#include "log.h"

//Funcion de ayuda para setear la bandera close on exec
int numBases[1];

char *directorio;
base *DBFILEDESC[30]={0};
char **claves_arr;
char **valores_arr;
int num_elementos_arr;
int TEST_ITEMS = 0;
int main( int argc, char *argv[]) { 
	numBases[0]= 0;
	int sockfd;
	if(argc == 1){
		printf("Uso: ./servidor <directorio> <ip> <numero de puerto>\n");
		exit(-1);
	}

	if(argc < 4){
		printf( "por favor especificar el IP y un numero de puerto\n");
        exit(-1);
	}

	int puerto = atoi(argv[3]);
    	directorio = argv[1];

	//Direccion del servidor
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_servidor.sin_family = AF_INET;		//IPv4
	direccion_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[2]) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

	

	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		printf("Error al inicializar el servidor\n");
        exit(-1);
	}		
	
	paquete *pqt;
	int rc;
	while(1){
		int clfd = accept(sockfd, NULL, NULL);
		
		if(clfd >0){				    
			printf("Mesaje recibido:\n");
			pthread_t tid;
			
			pqt = (paquete*)malloc(sizeof(paquete));
			
			pqt->lista = DBFILEDESC;
			pqt->numBases = numBases;
			pqt->directorio = directorio;
			pqt->clfd = clfd;
			
			rc = pthread_create(&tid,NULL,evaluar_Mensaje,(void*)pqt);
			if(rc){
				printf("ERROR\n");
				exit(-1);
			}
		}
	}
	exit(1);
}
