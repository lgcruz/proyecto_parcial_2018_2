#include "liblogdb.h" 

conexionlogdb *conectar_db(char *ip, int puerto){
	int sockfd;

	//Direccion del servidor
	struct sockaddr_in direccion_cliente;

	memset(&direccion_cliente, 0, sizeof(direccion_cliente));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_cliente.sin_family = AF_INET;		//IPv4
	direccion_cliente.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_cliente.sin_addr.s_addr = inet_addr(ip) ;	//Nos tratamos de conectar a esta direccion

	//AF_INET + SOCK_STREAM = TCP

	if (( sockfd = connect_retry( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) { 
		printf("falló conexión\n"); 
		exit(-1);
	} 	         

	conexionlogdb *obj = (conexionlogdb*)malloc(sizeof(conexionlogdb));
	obj->sockdf = sockfd;
	obj->ip = ip;
	obj->puerto = puerto;
	obj->id_sesion = -1;	
	
	return obj; 
}	
