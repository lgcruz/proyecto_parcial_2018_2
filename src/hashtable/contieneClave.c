#include "hashtable.h"

//Devuelve 1 si la clave existe en la hashtable, 0 si no.
int contieneClave(hashtable *tabla, char *clave){
    unsigned long resultado_hash = hash((unsigned char*)clave);
    int clave_h = resultado_hash % tabla->numeroBuckets;
    
    if(tabla->buckets[clave_h]==NULL){
        return 0;
    }else{
        objeto *objInterno = tabla->buckets[clave_h];
        while(objInterno!=NULL){
            if (strcmp((const char *)objInterno->clave,(const char *)clave)==0){
                return 1;
            }else{
                objInterno=objInterno->siguiente;
            }
        }
        return 0;
    }
}
