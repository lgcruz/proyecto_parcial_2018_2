#include "hashtable.h"

//Devuelve un arreglo de punteros con todos los valores dentro de la hash table
//El numero de elementos debe devolverse en la variable conteo
void **valores(hashtable *tabla, int *conteo){
    void **valores = (void **)malloc(sizeof(void *)*numeroElementos(tabla));
    int cont=0;
    for(int i=0;i<tabla->numeroBuckets;i++){
        if(tabla->buckets[i]!=NULL){
            objeto *objInterno = tabla->buckets[i];
            while(objInterno!=NULL){
                valores[cont++]=objInterno->valor;
                objInterno=objInterno->siguiente;
            }
        }
    }
    *conteo=numeroElementos(tabla);
    return valores;
}
