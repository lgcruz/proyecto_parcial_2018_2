#include "hashtable.h"


void put(hashtable *tabla, char *clave, void *valor){

	objeto *objIn=(objeto*)malloc(sizeof(objeto));
	objIn->clave = clave;
	objIn->valor = valor;
	objIn->siguiente = NULL;


	unsigned long clave_h = hash((unsigned char*)clave);


	int pos = clave_h % tabla->numeroBuckets;
	//printf("%lu %d\n",clave_h,pos);
	if (tabla->buckets[pos]==NULL){
		tabla->buckets[pos]=objIn;
		tabla->elementos++;
	}else{
		objeto *obj = tabla->buckets[pos];
		//printf("se ha agregado a la cola\n");
		
		while((obj->siguiente!=NULL) && (strcmp(obj->clave,clave)!=0)){
			obj = obj->siguiente;
		}
		if((strcmp(obj->clave,clave)==0)){
			obj->valor=valor;
		}else{
			obj->siguiente=objIn;
			tabla->elementos++;
		}
	}
}
