#include "hashtable.h"


//Devuelve valor asociado a clave, sin sacarlo de la hashtable
void *get(hashtable *tabla, char *clave){
    
    unsigned long resultado_hash = hash((unsigned char*)clave);
    int clave_h = resultado_hash % tabla->numeroBuckets;
    
    if(tabla->buckets[clave_h]==NULL){
        return NULL;
    }else{
        objeto *objInterno = tabla->buckets[clave_h];
        while(objInterno!=NULL){
            if (strcmp((const char *)objInterno->clave,(const char *)clave)==0){
                return objInterno->valor;
            }else{
                objInterno=objInterno->siguiente;
            }
        }
        return NULL;
    }
}
