#include "hashtable.h"

int numeroAzar2(int ini, int fin);
int numeroAzar2(int ini, int fin){
	int semilla = time(NULL);
    srand(semilla);
	int numero = rand() % ( (fin-1) + 1 - ini) + 0;
	return numero;
}

hashtable *crearHashTable(int numeroBuckets){
	hashtable *tabla = (hashtable*)malloc(sizeof(hashtable));
	tabla->numeroBuckets = numeroBuckets;
	tabla->elementos = 0;
	tabla->id = numeroAzar2(1,100);
	tabla->buckets = (objeto**)malloc(sizeof(objeto*)*numeroBuckets);

	return tabla;
}
