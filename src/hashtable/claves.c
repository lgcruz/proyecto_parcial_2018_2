#include "hashtable.h"

char **claves(hashtable *tabla, int *conteo){
	char **claves = (char **)malloc(sizeof(char *)*numeroElementos(tabla));
    	int cont=0;
    	for(int i=0;i<tabla->numeroBuckets;i++){
        	if(tabla->buckets[i]!=NULL){
            		objeto *objInterno = tabla->buckets[i];
            		while(objInterno!=NULL){
                		claves[cont++]=objInterno->clave;
                		objInterno=objInterno->siguiente;
            		}
        	}
    	}
    	*conteo=numeroElementos(tabla);
    	return claves;
}

