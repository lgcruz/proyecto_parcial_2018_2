#include <stdio.h>
#include "hashtable.h"

unsigned long hash(unsigned char *str){
	unsigned long hash = 5381;
	int c = 0;

	while((c = *str++)){
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c*/
	}
	return hash;
}
