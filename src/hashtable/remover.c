#include "hashtable.h"



void *remover(hashtable *tabla, char *clave){

	unsigned long resultado_hash = hash((unsigned char*)clave);
    	int clave_h = resultado_hash % tabla->numeroBuckets;
    	void* ptr = get(tabla,clave);
    	if(ptr==NULL){
        	return NULL;
    	}else{
        	objeto *objInterno1 = tabla->buckets[clave_h];
		objeto *objInterno2 = objInterno1->siguiente;
		if (strcmp((const char *)objInterno1->clave,(const char *)clave)==0){
			tabla->buckets[clave_h]=objInterno2;
			//free(objInterno1->valor);
			free(objInterno1->clave);
			free(objInterno1);
			
			tabla->elementos--;
                	return ptr;
            	}
        	while(objInterno2!=NULL){
            		if (strcmp((const char *)objInterno2->clave,(const char *)clave)==0){
				
				objInterno1->siguiente=objInterno2->siguiente;
				//free(objInterno2->valor);
				free(objInterno2->clave);
                		free(objInterno2);
				tabla->elementos--;
                		return ptr;
            		}else{
				objInterno1=objInterno1->siguiente;
                		objInterno2=objInterno2->siguiente;
            		}
        	}
        	return NULL;
	}

}
