#include "hashtable.h"


//Borrar todos los elementos de la hashtable, sin devolverlos (liberando la memoria de cada objeto)
void borrar(hashtable *tabla){

    	tabla->elementos=0;
    	
    	for(int i=0;i<tabla->numeroBuckets;i++){
        	if(tabla->buckets[i]!=NULL){
            		objeto *objInicial=tabla->buckets[i];
			objeto *objSecundario=tabla->buckets[i]->siguiente;
           		if(objInicial->siguiente==NULL){
				free(tabla->buckets[i]);
                		tabla->buckets[i]=NULL;
            		}else{
				tabla->buckets[i]=NULL;
                		while(objInicial!=NULL){
					free(objInicial->valor);
					free(objInicial->clave);
					//free(objInicial->siguiente);
                    			free(objInicial);
					objInicial=NULL;
                    			objInicial=objSecundario;
					if(objInicial!=NULL)objSecundario=objInicial->siguiente;
                		}
            		}
        	}
    	}

}
