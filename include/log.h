#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include <pthread.h>

#include <hashtable.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 

//Estructuras 
typedef struct baseObj{
	char *nombre;
	int desface;
	char* directorio;
	int clfd;               
	hashtable *table;    
}base;
typedef struct mnjObj{
	char *buff;
	base** lista;
	int *numBases;
	int clfd;
	char* directorio; 
}paquete;

void set_cloexec(int fd);
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen);
char *conectar_db(char *ip, char *puerto);

char *abrir_db(char *nombre_db,char* directorio,base** lista,int *numBases);
char *crear_db(char *nombre_db,char* directorio);
char *put_val(char *clave,char *valor,base* lista);//
char *get_val(base* lista, char *clave);
char *eliminar(base* lista,char *clave);//
char *cerrar_db(base* lista);
void *evaluar_Mensaje(void* obj);
char *compactar(int fd,char *directorio,hashtable *indices);

#endif



